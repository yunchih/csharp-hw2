﻿using System;
using System.Threading;

namespace hw2
{
    class Program
    {
        static void Main(string[] args)
        {
            int numPlayers;
            // Loop until user gives correct input
            while(true)
            {
                Console.Write("請輸入玩家數量（含自己）： ");
                if(int.TryParse(Console.ReadLine(), out numPlayers)
                    && numPlayers > 0 && numPlayers <= 52)
                    break;
            }

            Game game = new Game(numPlayers);
            game.Play();
        }
    }

}
