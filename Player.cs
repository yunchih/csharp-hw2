using System;
using System.Collections.Generic;

namespace hw2
{
    public class Player
    {
        public LinkedList<int> Cards;
        public int ID;
        public bool IsParticipant;

        public Player(int playerID, int[] cards, bool isParticipant)
        {
            Cards = new LinkedList<int>(cards);
            IsParticipant = isParticipant;
            ID = playerID;
            Pair();
        }

        public bool HasWon()
        {
            return Cards.Count == 0;
        }

        public int Draw()
        {
            // Randomly draw a card
            int targetCardID = new Random().Next(0, Cards.Count);
            int currentID = 0;

            LinkedListNode<int> currentCard = Cards.First;
            while(currentID != Cards.Count)
            {
                if(currentID == targetCardID)
                {
                    int drawn = currentCard.Value;
                    Cards.Remove(currentCard);
                    return drawn;
                }

                currentID += 1;
                currentCard = currentCard.Next;
            }

            // won't reach here
            return -1;
        }

        public void Add(int card)
        {
            Cards.AddLast(card);
        }

        public bool Pair()
        {
            LinkedListNode<int> card = Cards.First;
            while(card != Cards.Last)
            {
                int currentID = card.Value;
                int cardNum = currentID % 13;
                for(int i = 0; i < 4; i++)
                {
                    int targetID = 13 * i + cardNum;
                    // Don't pair Joker!
                    if(targetID != 0 && currentID != 0 &&
                       targetID != currentID && Cards.Contains(targetID))
                    {
                        Cards.Remove(card);
                        Cards.Remove(targetID);
                        return Pair();
                    }
                }

                card = card.Next;
            }

            return true;
        }

        public string DisplayCards()
        {
            string output = "| ";
            foreach(int card in Cards)
            {
                string s = Card.MakeString(card);
                output += $" {s} |";
            }

            return output + "\n";
        }
    }
}
