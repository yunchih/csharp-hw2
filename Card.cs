using System;

namespace hw2
{
    public static class Card
    {
        public static readonly string[] CardsTypesStr = { "♠", "♥", "♦", "♣", "🃏"};
        public enum CardsTypes { Spade, Heart, Diamond, Club, Joker };

        // id = 0: joker
        // id = 1: spade1
        // id = 2: spade2
        public static string MakeString(int id)
        {
            if(id > 52) // error
                throw new ArgumentException();
            if(id == 0)
                return CardsTypesStr[(int)CardsTypes.Joker];
            return CardsTypesStr[(id-1)/13] + EachMakeString(id);
        }

        public static string EachMakeString(int id)
        {
            id = id % 13;
            if(id == 11)
                return "J";
            if(id == 12)
                return "Q";
            if(id == 0)
                return "K";
            return id.ToString();
        }

        public static bool IsJoker(int id)
        {
            return id == 0;
        }
    }
}
