using System;
using System.Threading;
using System.Collections.Generic;

namespace hw2
{
    public class Game
    {
        public const int waitNextTime = 2000; // 2000 milliseconds
        public const int waitUserDrawn = 1200; // 2000 milliseconds
        public const int waitUserDrawnJoker = (int)(waitUserDrawn * 1.4); // 2000 milliseconds

        public int NumPlayers;
        public int ParticipantID;

        private int[] Winners;
        private int NumWinners;

        private Player[] Players;

        public Game(int numPlayers)
        {
            NumPlayers = numPlayers;
            Players = new Player[numPlayers];
            Winners = new int[numPlayers];
            NumWinners = 0;
        }

        public void Play()
        {
            Initialize();

            int playerID = 0;
            while(true)
            {
                UpdateBoard(playerID);
                if(CheckParticipantLose() == true)
                {
                    DisplayParticipantLose();
                    break;
                }

                // Check if current player has won
                if(Players[playerID].Cards.Count == 0)
                {
                    AddWinner(playerID);

                    if(Players[playerID].IsParticipant == true)
                    {
                        DisplayParticipantWin();
                        break;
                    }
                }
                // Has not won
                else
                {

                    if(Players[playerID].IsParticipant == true)
                    {
                        // DisplayPromptParticipant(playerID);
                        int drawnCard = DrawCard(playerID);
                        // DisplayDrawnCard(drawnCard);
                        Players[playerID].Pair();
                    }
                    else
                    {
                        int drawnCard = DrawCard(playerID);
                        // Console.WriteLine("drawn card: {0}", drawnCard);
                        Players[playerID].Pair();
                    }
                }


                // Hop to next player
                // Jump to the first player if we meet the last one
                playerID = (playerID + 1) % NumPlayers;

                // Wait for a bit for user to see updated board
                if(Players[playerID].Cards.Count != 0)
                    Thread.Sleep(waitNextTime);
            }
        }

        private int DrawCard(int currentID)
        {
            int lastPlayerID = GetLastPlayer(currentID);
            int drawnCard = Players[lastPlayerID].Draw();
            if(drawnCard >= 0)
                Players[currentID].Add(drawnCard);
            return drawnCard;
        }

        public void Initialize()
        {
            InitializePlayers();
        }

        private void InitializePlayers()
        {
            // Randomly shuffle cards
            int[] cards = new int[53];
            for(int i = 0; i < 53; i++)
                cards[i] = i;
            new Random().Shuffle(cards);

            // Randomly choose participant ID
            // a number between 0 ... NumPlayers-1
            ParticipantID = new Random().Next(0, NumPlayers);

            // Create each players
            int cardsPerPlayer = (53 / NumPlayers);
            int cardsPerPlayerExtra = (53 % NumPlayers);
            for(int playerID = 0, from = 0, until; playerID < NumPlayers; playerID++)
            {
                until = from + cardsPerPlayer;

                // distribute remaining cards to players
                if(cardsPerPlayerExtra > 0)
                {
                    until += 1;
                    cardsPerPlayerExtra -= 1;
                }

                bool isParticipant = (ParticipantID == playerID);
                Players[playerID] =
                    new Player(
                        playerID,
                        cards.Slice(from, until),
                        isParticipant
                    );

                from = until;
            }
        }

        private void UpdateBoard(int currentPlayerID)
        {
            string cardsDisplay = GetCardsDisplay(currentPlayerID);
            string participantDeck = GetParticipantDeckDisplay();

            Console.Clear();
            Console.Write("各玩家目前持牌數量：\n\n");
            Console.Write(cardsDisplay);

            Console.Write("\n\n您手中的牌：\n\n");
            Console.Write(participantDeck);
        }

        private void DisplayPromptParticipant(int currentPlayerID)
        {
            int lastPlayerID = GetLastPlayer(currentPlayerID);
            int cardsCount = Players[lastPlayerID].Cards.Count;
            string stars = "  ";

            for(int i = 0; i < cardsCount; ++i)
               stars += (i+1).ToString() + ": 🂠   ";

            Console.WriteLine("\n對方的牌:");
            while(true)
            {
                Console.WriteLine(stars);
                Console.Write("請抽一張牌: ");

                int choice;
                if(int.TryParse(Console.ReadLine(), out choice) &&
                   choice <= cardsCount && choice > 0)
                    break;

                Console.WriteLine("請重新輸入!\n");
            }
        }

        private void DisplayDrawnCard(int cardID)
        {
            if(Card.IsJoker(cardID))
            {
                Console.WriteLine("你抽了 " + Card.MakeString(cardID)
                        + " 你是烏龜！ XD");
                Thread.Sleep(waitUserDrawnJoker);
            }
            else
            {
                Console.WriteLine("你抽了 " + Card.MakeString(cardID));
                Thread.Sleep(waitUserDrawn);
            }


        }
        private void DisplayParticipantWin()
        {
            Console.Write("恭喜你勝利！\n");
            Console.Write("你的名次是 {0}！\n\n", GetParticipantRanking());
        }

        private void DisplayParticipantLose()
        {
            Console.Write("恭喜你輸了！\n\n");
        }

        private string GetParticipantDeckDisplay()
        {
            return Players[ParticipantID].DisplayCards();
        }

        private string GetCardsDisplay(int currentPlayerID)
        {
            // Generate header
            string header = "|";
            for(int playerID = 0; playerID < NumPlayers; playerID++)
            {
                string identity = "C" + playerID.ToString();
                if(playerID == ParticipantID)
                    identity = "You";
                identity = identity.PadLeft(3);

                if(playerID == currentPlayerID)
                    identity = "🢂 🢂 " + identity + "🢀 🢀 ";
                else
                    identity = "  " + identity + "  ";

                header += $" {identity} |";
            }

            // Generate separator
            string separator = string.Join("-", new string[header.Length + 1]);

            // Generate card display
            string body = "|";
            for(int playerID = 0; playerID < NumPlayers; playerID++)
            {
                string count = Players[playerID].Cards.Count.ToString();
                count = "  " + count.PadLeft(3) + "  "; // add some spaces for alignment
                body += $" {count} |";
            }

            // Generate whose turn
            /*
            string whoseTerm = "";
            for(int playerID = 0; playerID < NumPlayers; playerID++)
            {
                if(playerID == currentPlayerID)
                {
                    whoseTerm += "  ↑↑↑  ";
                    break;
                }

                whoseTerm += "      "; // 5 spaces
            }
            */

            // Join each part in separate lines
            return string.Join("\n", new string[] {header, separator, body});
        }

        private int GetLastPlayer(int currentPlayerID)
        {
            int lastPlayerID = currentPlayerID;
            while(true)
            {
                lastPlayerID = (NumPlayers + lastPlayerID - 1) % NumPlayers;
                if(Players[lastPlayerID].HasWon() == false)
                    return lastPlayerID;
            }
        }

        private void AddWinner(int playerID)
        {
            for(int i = 0; i < NumWinners; i++)
                if(Winners[i] == playerID)
                    return;

            Winners[NumWinners] = playerID;
            NumWinners += 1;
        }

        private bool CheckParticipantLose()
        {
            return NumWinners == NumPlayers - 1 &&
                    Players[ParticipantID].HasWon() == false;
        }

        private int GetParticipantRanking()
        {
            int i;
            for(i = 0; i < NumWinners; i++)
                if(Winners[i] == ParticipantID)
                    break;
            return i + 1;
        }
    }
}
